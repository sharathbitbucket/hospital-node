const BODYPARSER = require('body-parser');
const EXPRESS = require('express');
const CORS = require('cors');
const CONFIG = require('./config/config');

const PORT = CONFIG.PORT;
const APP = EXPRESS();

const USERS = require('./routes/users');
const DBCONNECTER = require('./routes/dbconnect');

APP.use(CORS());
APP.use(BODYPARSER.json());
APP.use('/api', USERS);

DBCONNECTER.connection().then(response =>{
    APP.listen(PORT , console.log(`server is running on port ${PORT}`));
});

module.exports= APP;


