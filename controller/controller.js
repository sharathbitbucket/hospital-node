const MODEL = require('../models/model');
const departmentModel = require('../models/department');
const doctorsModel = require('../models/doctor');

let controller = {
    patientAppointments : patientAppointments,
    getPatients : getPatients,
    addDepartments : addDepartments,
    getDepartments : getDepartments,
    postDoctors: postDoctors,
    getDoctors: getDoctors
} // functions to be exported

function patientAppointments(req, res){
    if(req && req.body){       
        MODEL.appointments(req, res);
    } else {
        res.send('Someting went wrong, please try again....');
    }
} 

function getPatients(req, res){
    if(req ){       
        MODEL.getPatientsData(req, res);
    } else {
        res.send('Someting went wrong, please try again....');
    }
} 

function addDepartments(req, res){
    if(req && req.body){       
        departmentModel.addingDepartments(req, res);
    } else {
        res.send('Someting went wrong, please try again....');
    }
} 

function getDepartments(req, res){
    if(req){       
        departmentModel.getDepartments(req, res);
    } else {
        res.send('Someting went wrong, please try again....');
    }
} 

function postDoctors(req, res){
    if(req){       
        doctorsModel.postDoctors(req, res);
    } else {
        res.send('Someting went wrong, please try again....');
    }
} 

function getDoctors(req, res){
    if(req){       
        doctorsModel.getDoctors(req, res);
    } else {
        res.send('Someting went wrong, please try again....');
    }
} 


module.exports = controller;