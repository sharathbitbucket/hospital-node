let departmentals = {
    department :{
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required: true
    }
   
}
module.exports = departmentals;