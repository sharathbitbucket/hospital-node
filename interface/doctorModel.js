let doctors = {
    doctorname :{
        type: String,
        trim: true,
        required: true
    },
    doctordepartment: {
        type: String,
        trim: true,
        required: true
    },
    mobile: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true
    }
   
}
module.exports = doctors;