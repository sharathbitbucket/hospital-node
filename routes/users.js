const EXPRESS = require('express');
const ROUTER = EXPRESS.Router();
const CONTROLLER = require('../controller/controller');


//appointments post page
ROUTER.post('/appointments', CONTROLLER.patientAppointments);

//get patients from appointments 
ROUTER.get('/getPatients', CONTROLLER.getPatients);

//Post Departments
ROUTER.post('/addingdepartments', CONTROLLER.addDepartments);

//Get Departments
ROUTER.get('/getdepartments', CONTROLLER.getDepartments);

//Post Doctors
ROUTER.post('/postdoctors', CONTROLLER.postDoctors);

//Get Doctors
ROUTER.get('/getdoctors', CONTROLLER.getDoctors);


module.exports = ROUTER;