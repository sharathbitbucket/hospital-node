const MONGOOSE = require('mongoose');
const CONFIG = require('../config/config');

let dbconnector = {
    connection: connection
}

async function connection() {
    
    return await MONGOOSE.connect(CONFIG.DB_URL, 
    { 
        useUnifiedTopology: true, 
        useNewUrlParser: true 
    }, 
    function(err) {
        if(err) {
            console.log('failed ' + err);
        } else {
            console.log('Succesfully connected with the mongodb');
        }
    });
}

MONGOOSE.connection.on('error', (err) => {
    console.log(err)
})

module.exports = dbconnector;